#include "RandomText.h"
#include "ExamplePlugin.h"

ExamplePlugin::ExamplePlugin()
{
    volatile auto registration = &qml_register_types_Example;
    Q_UNUSED(registration);
}

ExamplePlugin::~ExamplePlugin() {}

void ExamplePlugin::registerTypes(const char* url) {
    qmlRegisterType<RandomText>(url,1,0,"RandomText");
}
