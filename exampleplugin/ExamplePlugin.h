#ifndef EXAMPLEPLUGIN_H
#define EXAMPLEPLUGIN_H

#include <QQmlExtensionPlugin>
#include <QtQml>

void qml_register_types_Example();

class ExamplePlugin : public QQmlExtensionPlugin {
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "com.example.exampleplugin")

public:

  ExamplePlugin();
  ~ExamplePlugin();

  void registerTypes(const char* url);
};

// Important!  This includes some metadata which avoids a frustrating and opaque
// "Plugin verification data mismatch" error when attempting to use the plugin.
// And the include is needed for qmake to trigger the generation of the file!
//#include "ExamplePlugin.moc"
#endif // EXAMPLEPLUGIN_H
