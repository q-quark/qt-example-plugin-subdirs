#ifndef __random_text_h__
#define __random_text_h__

#include <QQuickItem>
#include <QString>
#include <QTimer>

class RandomText : public QQuickItem {

  Q_OBJECT

  Q_PROPERTY(QString text READ text NOTIFY textChanged)

public:

  RandomText();
  ~RandomText();

  QString text() const;

signals:

  void textChanged(const QString&);

private:
 
  QTimer _ticker;
  QString _text;
 
private slots:

  void changeText();
};

#endif
