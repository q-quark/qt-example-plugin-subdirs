#include <iostream>

#include <QDateTime>
#include <QGuiApplication>
#include <QQmlEngine>
#include <QQuickView>
#include <QtPlugin>
#include <QUrl>

#ifdef IOSQUIRKS
#include <QQmlExtensionPlugin>  // Needed for QTBUG-28357 idea below

Q_IMPORT_PLUGIN(ExamplePlugin)
#endif

int main(int argc,char* argv[])
{
  QGuiApplication app(argc,argv);

  srand(QDateTime::currentMSecsSinceEpoch());

  QQuickView viewer;

#ifdef LINUXQUIRKS
  // On Linux, use direct from the build area "next door"
  viewer.engine()->addImportPath(
    QCoreApplication::applicationDirPath()+"/../../exampleplugin/imports/"
  );
#endif

#ifdef MACXQUIRKS
  // On OSX use imports pulled into top level of bundle; executable is buried deeper
  viewer.engine()->addImportPath(
    QCoreApplication::applicationDirPath()+"/../../imports/"
  );
#endif

#ifdef IOSQUIRKS
  // On iOS use imports pulled into top level of bundle; executable sits there
  viewer.engine()->addImportPath(
    QCoreApplication::applicationDirPath()+"/imports/"
  );
#endif

  viewer.resize(QSize(480,480)); // Seems harmless on mobile

  viewer.setResizeMode(QQuickView::SizeRootObjectToView);

#ifdef IOSQUIRKS
  // Idea from https://bugreports.qt.io/browse/QTBUG-28357
  qobject_cast<QQmlExtensionPlugin*>(qt_static_plugin_ExamplePlugin().instance())->registerTypes("ExamplePlugin");
#endif

  viewer.setSource(QUrl("qrc:/qml/main.qml"));
  std::cerr << "QQuickView status after setSource is ";
  switch (viewer.status()) {
  case QQuickView::Null: 
    std::cerr << "Null";break;
  case QQuickView::Ready: 
    std::cerr << "Ready";break;
  case QQuickView::Loading: 
    std::cerr << "Loading";break;
  case QQuickView::Error: 
    std::cerr << "Error";break;
  default:
    std::cerr << "Unknown";break;
  }
  std::cerr << std::endl;

  viewer.show();
  return app.exec();
}

