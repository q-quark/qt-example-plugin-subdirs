TEMPLATE = app

TARGET = demoapp

CONFIG += release

CONFIG += c++11

QT += core qml gui quick

SOURCES += main.cpp
RESOURCES = resources.qrc

DESTDIR = build
OBJECTS_DIR = build/obj
MOC_DIR = build/moc
RCC_DIR = build/qrc

unix:!mac{
  DEFINES += LINUXQUIRKS
  QMAKE_CXXFLAGS += -std=c++11
}

macx {
  DEFINES += MACXQUIRKS
  QMAKE_CXXFLAGS += -std=c++11

  IMPORTS_PLUGIN.path = imports/ExamplePlugin
  IMPORTS_PLUGIN.files += $$files(../exampleplugin/imports/ExamplePlugin/*.dylib)
  IMPORTS_PLUGIN.files += ../exampleplugin/imports/ExamplePlugin/qmldir
  QMAKE_BUNDLE_DATA += IMPORTS_PLUGIN
}

ios {
  DEFINES += IOSQUIRKS

  IMPORTS_PLUGIN.path = imports/ExamplePlugin
  IMPORTS_PLUGIN.files += ../exampleplugin/imports-static/ExamplePlugin/qmldir  # NB needs the slightly tweaked qmldir from dynamic platforms
  QMAKE_BUNDLE_DATA += IMPORTS_PLUGIN


  QTPLUGIN += ExamplePlugin
  QML_IMPORT_PATH += ../exampleplugin/imports-static

  LIBS += ../exampleplugin/imports-static/ExamplePlugin/libexampleplugin.a  # Avoids undefined qt_static_plugin_ExamplePlugin symbol from main.cpp
}

cache()
