import QtQuick 2.0

import ExamplePlugin 1.0

Rectangle {
  anchors.fill: parent
  color: 'white'

  RandomText {
    id: randomtext
    visible: false
  }

  Column {
    anchors.fill: parent

    Text {
      width: parent.width
      height: parent.height/2.0
      horizontalAlignment: Text.AlignHCenter
      verticalAlignment: Text.AlignVCenter
      font.pixelSize: height/10.0
      text: 'What does the plugin say?'
    }
    Text {
      width: parent.width
      height: parent.height/2.0
      horizontalAlignment: Text.AlignHCenter
      verticalAlignment: Text.AlignVCenter
      font.pixelSize: height/10.0
      text: 'The plugin says\n"'+randomtext.text+'"'
    }
  }
}
