About this repository
=====================

This is an implementation of QMAKE SUBDIRS of https://bitbucket.org/timday/qt-example-plugin/src/master/

Can be run from QtCreator.
Tested `macos Monterey 12.4 with Qt 6.3.1` (built from source) and running on iPhone SE (3rd gen)

Original author: Tim Day.

Qt Plugin Example
=================


This is intended to be a minimal standalone demo of what it takes to get QML plugins to work in a QtQuick application on various platforms.

See Qt Forums thread at https://forum.qt.io/topic/56878/qml-plugin-on-ios for background.

The contents are:

* `exampleplugin/` : defines a RandomText QQuickItem which simply updates a `text` property with a random string every second.  The item isn't even intended to be displayed; just to provide evidence some native code has been loaded and is functioning as a citizen of the QML scene.
    * The plugin builds to `exampleplugin\imports\ExamplePlugin\` which contains the built lib and a trivial `qmldir` file.
* `demoapp/` : Trivial `QQuickView`-based application which loads a QML file `qml/main.qml` (via resources), but more importantly needs to setup whatever it takes for that QML's `import ExamplePlugin 1.0` to succeed.
    * The `main.qml` just imports ExamplePlugin and uses an instance of the RandomText item to provide some content (via property binding).


Running with `QT_DEBUG_PLUGINS=1` may be interesting.

Status on various platforms:

Linux
-----
Can be run from QtCreator.
Code is built with a `LINUXQUIRKS` macro defined, which is just used (by a `#ifdef`) to set the QmlEngine's import path (simply points to the `exampleplugin/imports` directory, relative to the built executable; unlike other platforms there is no copying stuff around/"deploying" into bundles).

Working with Qt5.5.0

Mac OSX
-------
Can be run from QtCreator.
Run it with `./demoapp/build/demoapp.app/Contents/MacOS/demoapp` or `open demoapp/build/demoapp.app`.
Just works; a window appears with updating text.

Code is built with a `MACXQUIRKS` macro defined, which is just used (by a `#ifdef`) to set the QmlEngine's import path to point at the imports in the top level of the deployed app bundle (see macx's `QMAKE_BUNDLE_DATA` in demoapp.pro file).

Using Qt5.4.1 because of a macdeployqt issue with Qt5.5.0 (reported; expected fixed in 5.5.1)

iOS
---
Can be run from QtCreator or open `demoapp/demoapp.xcodeproj` in XCode and run it on an iOS device.  Just works; but NB needs to be a proper arm architecture device; `attempting to build for simulator will have problems with the plugin not having an x86 build.`

Code is built with an `IOSQUIRKS` macro defined.

Working with Qt6.3.1

Open questions
==============

* Currently iOS needs registerTypes to be invoked explicitly; seems bogus.  Raise as a QTBUG?

* Is it possible to put (dynamic) plugins (including native code .so/.dylib) into a qrc?

* Is it possible to use dynamic plugins on iOS by any means?  (Or at least after iOS version 8?)

* Not entirely clear what's going on when set `CONFIG += debug` in `.pro` files (instead of `release`); but iOS build no longer works then.

Screenshots
===========

![Linux screenshot](screenshots/Linux-20150817.png)

![OSX screenshot](screenshots/OSX-20150817.png)

![iOS screenshot](screenshots/iOS-20150817.png)
